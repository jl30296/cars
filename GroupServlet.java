/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.GroupTable;
import ejb.GroupTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Controller;

/**
 *
 * @author Vlera
 */
public class GroupServlet extends HttpServlet {

    @EJB
    private GroupTableFacade groupTableFacade;
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        this.request = request;
        this.response = response;
        Controller.invokeMethods(this, request, response);
    }

    public void addGroupView() {
        try {
            request.getRequestDispatcher("Group/AddGroup.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addGroup() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            System.out.println(name + ";;;;;;;;;;;;;;;");
            GroupTable group = new GroupTable();
            group.setName(name);
            group.setStatus((short) 1);

            boolean result = groupTableFacade.addGroup(group);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void listGroupView() {
        try {
            List<GroupTable> list=groupTableFacade.listGroups();
            request.setAttribute("list", list);
            
            request.getRequestDispatcher("Group/ListGroups.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteGroup(){
        try(PrintWriter out=response.getWriter()){
        int id=Integer.parseInt(request.getParameter("id"));
        GroupTable group=groupTableFacade.find(id);
        
        boolean result=groupTableFacade.deleteGroup(group);
        if(result){
            out.println("success");
        }else{
            out.println("error");
        }
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    public void editGroupView(){
        try {
            int id=Integer.parseInt(request.getParameter("id"));
            GroupTable group=groupTableFacade.find(id);
            request.setAttribute("group", group);
            request.getRequestDispatcher("Group/EditGroup.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editGroup(){
        try(PrintWriter out=response.getWriter()){
        String name=request.getParameter("name");
        int globalId=Integer.parseInt(request.getParameter("globalId"));
        
        GroupTable group=groupTableFacade.find(globalId);
        group.setName(name);
        
        boolean result=groupTableFacade.editGroup(group);
        if(result){
            out.println("success");
        }
        else{
            out.println("error");
        }
        } catch (IOException ex) {
            Logger.getLogger(GroupServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
