/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.ModelTable;
import ejb.ModelTableFacade;
import ejb.TypeTable;
import ejb.TypeTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Controller;

/**
 *
 * @author Vlera
 */
public class TypeServlet extends HttpServlet {

    @EJB
    private ModelTableFacade modelTableFacade;
    @EJB
    private TypeTableFacade typeTableFacade;
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        this.request = request;
        this.response = response;

        Controller.invokeMethods(this, request, response);

    }

    public void addTypeView() {
        try {
            List<ModelTable> listModel = modelTableFacade.findAll();
            request.setAttribute("listModels", listModel);
            request.getRequestDispatcher("Type/AddType.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(TypeServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TypeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addType() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            int selecti = Integer.parseInt(request.getParameter("selecti"));
            ModelTable model = modelTableFacade.find(selecti);

            TypeTable type = new TypeTable();
            type.setName(name);
            type.setModelId(model);
            type.setStatus((short) 1);

            boolean res = typeTableFacade.addType(type);
            if (res) {
                out.print("success");
            } else {
                out.print("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(TypeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void listTypeView() {
        try {
            List<TypeTable> list = typeTableFacade.listTypes();
            request.setAttribute("list", list);

            request.getRequestDispatcher("Type/listTypes.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(TypeTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TypeTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteType() {
        try (PrintWriter out = response.getWriter()) {
            int id = Integer.parseInt(request.getParameter("id"));
            TypeTable type = typeTableFacade.find(id);

            boolean result = typeTableFacade.deleteType(type);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(TypeTable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void editTypeView() {
        try {
            List<ModelTable> listModel = modelTableFacade.findAll();
            request.setAttribute("listModels", listModel);

            int id = Integer.parseInt(request.getParameter("id"));
            TypeTable type = typeTableFacade.find(id);
            request.setAttribute("type", type);
            request.getRequestDispatcher("Type/EditType.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(TypeTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TypeTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editType() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            int globalId = Integer.parseInt(request.getParameter("globalId"));
            int modelId = Integer.parseInt(request.getParameter("modelId"));
            
            ModelTable model = modelTableFacade.find(modelId);
              
            TypeTable type = typeTableFacade.find(globalId);
            type.setName(name);
            type.setModelId(model);
            boolean result = typeTableFacade.editType(type);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(TypeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
