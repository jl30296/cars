/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.CarTable;
import ejb.CarTableFacade;
import ejb.TypeTable;
import ejb.TypeTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Controller;

/**
 *
 * @author admin
 */
public class VehicleServlet extends HttpServlet {

    @EJB
    private TypeTableFacade typeTableFacade;
    @EJB
    private CarTableFacade carTableFacade;

    private HttpServletRequest request;
    private HttpServletResponse response;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        this.request = request;
        this.response = response;
        Controller.invokeMethods(this, request, response);
    }

    public void addVehicleView() {

        try {
            List<TypeTable> list = carTableFacade.listTypes();
            request.setAttribute("list", list);
            request.getRequestDispatcher("Vehicle/AddVehicle.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(VehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addVehicle() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            String color = request.getParameter("color");
            String registration = request.getParameter("registration");

            int selecti = Integer.parseInt(request.getParameter("type"));
            TypeTable type = typeTableFacade.find(selecti);

            int price = Integer.parseInt(request.getParameter("price"));
            CarTable car = new CarTable();
            // int selecti = Integer.parseInt(request.getParameter("selecti"));

            car.setName(name);
            car.setColor(color);
            car.setRegistration(registration);
            car.setPrice(price);
            car.setTypeId(type);
            car.setStatus((short) 1);

            boolean res = carTableFacade.addCar(car);
            if (res) {
                out.print("success");
            } else {
                out.print("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(VehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void listVehicleView() {
        try {
            List<CarTable> list = carTableFacade.listVehicles();
            request.setAttribute("list", list);

            request.getRequestDispatcher("Vehicle/ListVehicle.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(CarTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CarTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteVehicle() {
        try (PrintWriter out = response.getWriter()) {
            int id = Integer.parseInt(request.getParameter("id"));
            CarTable vehicle = carTableFacade.find(id);

            boolean result = carTableFacade.deleteCar(vehicle);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(CarTable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void editVehicleView() {
        try {
            List<TypeTable> list = typeTableFacade.listTypes();
            request.setAttribute("type", list);

            int id = Integer.parseInt(request.getParameter("id"));
            CarTable car = carTableFacade.find(id);
            request.setAttribute("car", car);
            request.getRequestDispatcher("Vehicle/EditVehicle.jsp").include(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(CarTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CarTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editVehicle() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            String registration = request.getParameter("registration");
            String color = request.getParameter("color");
            int globalId = Integer.parseInt(request.getParameter("globalId"));
            int selecti = Integer.parseInt(request.getParameter("type"));
            int price1 = Integer.parseInt(request.getParameter("price"));
            
            
            TypeTable type = typeTableFacade.find(selecti);

            CarTable car = carTableFacade.find(globalId);
            
            car.setName(name);
            car.setRegistration(registration);
            car.setColor(color);
            car.setTypeId(type);
            
            car.setPrice(price1);

            boolean result = carTableFacade.editCar(car);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(VehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
