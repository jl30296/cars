/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import ejb.GroupTable;
import ejb.GroupTableFacade;
import ejb.UserTable;
import ejb.UserTableFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.Controller;

/**
 *
 * @author Vlera
 */
public class UserServlet extends HttpServlet {

    @EJB
    private UserTableFacade userTableFacade;
    @EJB
    private GroupTableFacade groupTableFacade;

    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        this.request = request;
        this.response = response;

        Controller.invokeMethods(this, request, response);

    }

    public void addUserView() {
        try {
            List<GroupTable> list = groupTableFacade.listGroups();
            request.setAttribute("list", list);
            request.getRequestDispatcher("User/AddUser.jsp").forward(request, response);//Ku eshte pamja e krijuar
        } catch (ServletException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addUser() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            String surname = request.getParameter("surname");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            int groupId = Integer.parseInt(request.getParameter("groupId"));
            GroupTable group = groupTableFacade.find(groupId);

            String type = request.getParameter("type");
            UserTable user = new UserTable();

            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);
            user.setStatus((short) 1);
            user.setGroupId(group);

            boolean res = userTableFacade.addUser(user);
            if (res) {
                out.print("success");
            } else {
                out.print("error");
            }

        } catch (IOException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listUserView() {
        try {
            List<UserTable> userList = userTableFacade.findAll();
            request.setAttribute("userList", userList);
            request.getRequestDispatcher("User/ListUser.jsp").include(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteUser() {
        try (PrintWriter out = response.getWriter()) {
            int id = Integer.parseInt(request.getParameter("id"));
            UserTable user = userTableFacade.find(id);

            boolean result = userTableFacade.deleteUser(user);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(UserTable.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void editUserView() {
        try {

            List<GroupTable> list = groupTableFacade.listGroups();
            request.setAttribute("list", list);
            int id = Integer.parseInt(request.getParameter("id"));
            UserTable user = userTableFacade.find(id);
            request.setAttribute("user", user);
            request.getRequestDispatcher("User/EditUser.jsp").include(request, response);

        } catch (ServletException ex) {
            Logger.getLogger(UserTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editUser() {
        try (PrintWriter out = response.getWriter()) {
            String name = request.getParameter("name");
            String surname = request.getParameter("surname");
            String password = request.getParameter("password");
            String username = request.getParameter("username");
            int globalId = Integer.parseInt(request.getParameter("globalId"));
            int groupId = Integer.parseInt(request.getParameter("groupId"));

            GroupTable model = groupTableFacade.find(groupId);

            UserTable user = userTableFacade.find(globalId);
            user.setName(name);
            user.setGroupId(model);
            user.setPassword(password);
            user.setUsername(username);
            user.setSurname(surname);

            boolean result = userTableFacade.editUser(user);
            if (result) {
                out.println("success");
            } else {
                out.println("error");
            }
        } catch (IOException ex) {
            Logger.getLogger(UserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
